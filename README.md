# PaperSlicer
A slicer for images and PDFs to be printed by a 3D-printer with a pen.
Don't take this project too seriously, it's more of a practical joke.
But the created images have some kind of charm.

## Usage
It has 3 modes:
- `dots`: Draws individual dots
    
    Currently the default mode. Looks nice, but takes ages to print because it has so many z-moves.
- `horizontal`: Draws horizontal lines

    Still looks nice and is significantly faster than `dots`.
    The pen should not have too much play in the x-direction because it draws on the forward and backward pass, which can cause alternating layer shifts.
- `concentric`: Draws the outline of the image

    A bit faster than `horizontal`, but doesn't look as nice with complex images.
    Needs a stable pen holder because it moves in all directions, wich can cause blank spots when the pen is not in the right position.

### Command-line Arguments

- `-i`, `--input`: Input file (required, accepts pdf or png)
- `-o`, `--output`: Output file (default: output.gcode)
- `-m`, `--mode`: Painting mode (dots, horizontal, concentric; default: dots)
- `-s`, `--speed`: Movement speed (default: 100)
- `-a`, `--acceleration`: Movement acceleration (default: 1000)
- `-Y`, `--overwrite`: Automatically confirm overwriting existing file (flag, default: False)
- `-c`, `--start_coordinates`: Start coordinates in the format x y (default: 0, 0)
- `-d`, `--dimensions`: Printed dimensions in the format x y (default: 210, 297 the size of a DIN A4 paper)
- `-r`, `--resolution`: Resolution in Dots per mm (default: 2)
- `-p`, `--pen_offset`: Pen offset in the format x y z (default: 0, 0, 0)
- `-l`, `--lift`: Height of the lifted pen (default: 2)
- `--custom_start`: Custom start GCode file (default: none)
- `--custom_end`: Custom end GCode file (default: none)
- `--custom_next_page`: Custom next page GCode file (default: none)

### Example
```bash
python paperslicer.py
-i examples/Benchy.png  # I want to print the image Benchy.png
-o examples/Benchy      # and store the GCode in Benchy.gcode
-d 128 103              # with a size of 128x103mm
-c 165 18               # starting at 165mm in x and 18mm in y
-p 10 30 3              # the pen is at 0 when the nozzle is at 10, 30, 3 mm
-l 1                    # Lift 1 mm when not drawing
-r 2                    # 2 Lines per mm, roughly the inverse of the pen width
-s 50                   # draw with 50mm/s
-Y                      # Overwrite the output file without asking
-m horizontal           # Draw horizontal lines
--custom_start ressources/start.gcode
--custom_end ressources/end.gcode
```
If the output does not end in `.gcode`, it will be appended.

If your print head can go into the negative x or y direction, you can also put the pen in front of the nozzle and set a negative pen offset.
In the Z coordinate, it's not a good idea to go below 0 because the print head will crash into the bed.

When the pen touches the paper while being lifted, you can increase the lift height.

The resolution can also be below 1 with a thick pen. 
A higher resolution will avoid blank spots but will also use more ink.

My custom start GCode starts Bed leveling, drives to the starting position and waits 30 seconds to insert the pen.
I cant insert the Pen before starting the print because my BLTouch sits higher than the pen.

My custom end GCode moves the print head out of the way and disables the motors.

Before starting the print, I add the paper to the bed.
To increase the bed adhesion, I use a glue stick drawing a box and diagonals before adding the paper.
Don't use much glue, or the paper will tear when removing it.

While printing, I use the Z-offset tuning to adjust how firmly the pen is pressed against the paper.

After the print is finished I use a ruler to remove the paper without tearing it.

## License
This project is licensed under the [CC-BY 4.0 License](https://creativecommons.org/licenses/by/4.0/).

**You are free to:**
- **Share** — copy and redistribute the material in any medium or format
- **Adapt** — remix, transform, and build upon the material for any purpose, even commercially.
- The licensor cannot revoke these freedoms as long as you follow the license terms.

**Under the following terms:**
- **Attribution** — You must give appropriate credit, provide a link to the license, and indicate if changes were made.
- You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- **No additional restrictions** — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

## Attribution
I do not own the images in the examples folder (except Test.png).

### 3D Benchy
#3DBenchy by Creative-Tools.com is licensed under a Creative Commons Attribution-NoDerivatives 4.0 International License.
See https://www.3dbenchy.com/license/.

Sharing images of #3DBenchy is permitted, even with changes and modifications.
I have removed the GCode file because I don't know if it counts as derivative work.
You can generate them yourself with the command given above.

### Bad Apple
The image is from the shadow animation created by Alstroemeria Records released on YouTube.
https://www.youtube.com/watch?v=i41KoE0iMYU

## Results
Using lines:
![Benchy_Lines.jpg](images/Benchy_Lines.jpg)

Using dots:
![BadApple Dots.jpg](images/BadApple_Dots.jpg)

Using concentric:
![BadApple Concentric.jpg](images/BadApple_Concentric.jpg)