import argparse
from datetime import datetime

import pdf2image
import numpy as np
from PIL import Image


def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description="Example script with command-line parameters")
    parser.add_argument("-i", "--input", type=str, help="Input file (pdf, png)", required=True)
    parser.add_argument("-o", "--output", type=str, help="Output file (gcode)", default="output.gcode")
    parser.add_argument("-m", "--mode", type=str, help="Painting mode (dots, horizontal, concentric)", default="dots")
    parser.add_argument("-s", "--speed", type=float, help="Movement speed", default=100)
    parser.add_argument("-a", "--acceleration", type=float, help="Movement acceleration", default=1000)
    parser.add_argument("-Y", "--overwrite", action="store_true",
                        help="Set to automatically confirm overwriting existing file",
                        default=False)
    parser.add_argument("-c", "--start_coordinates", nargs=2, type=float, help="Start coordinates (x, y)",
                        default=[0, 0])
    parser.add_argument("-d", "--dimensions", nargs=2, type=float, help="Printed dimensions (x, y)",
                        default=[210, 297])  # DIN A4
    parser.add_argument("-r", "--resolution", type=float, help="Resolution in Dots per mm", default=2)
    parser.add_argument("-p", "--pen_offset", nargs=3, type=float, help="Pen offset (x, y, z)", default=[0, 0, 0])
    parser.add_argument("-l", "--lift", type=float, help="Height of the lifted Pen", default=2)
    parser.add_argument("--custom_start", type=str, help="Custom start GCode file", default="none")
    parser.add_argument("--custom_end", type=str, help="Custom end GCode file", default="none")
    parser.add_argument("--custom_next_page", type=str, help="Custom next page GCode file", default="none")
    args = parser.parse_args()

    # Check if mode is valid
    if args.mode not in ["dots", "horizontal", "concentric"]:
        print("Error: Invalid mode, choose from dots, horizontal, concentric")
        return

    # TODO: Print Error if file is not found
    # Check if input files exists
    if not check_file_exists(args.input):
        return
    if args.custom_start != "none" and not check_file_exists(args.custom_start):
        return
    if args.custom_end != "none" and not check_file_exists(args.custom_end):
        return
    if args.custom_next_page != "none" and not check_file_exists(args.custom_next_page):
        return

    # Add output file extension if not present
    if not args.output.endswith(".gcode"):
        args.output += ".gcode"

    # Create output file
    try:
        with open(args.output, "x") as output_file:
            try:
                slice_paper(output_file, args)
            except Exception as e:
                print(f"Error: Error while slicing {args.input} to {args.output}")
                print(e)
                return
    except FileExistsError:
        # warn if file exists
        if not args.overwrite:
            print(f"Warning: File {args.output} already exists and will be overwritten\n(Y) to continue ")
            if input().upper() != "Y":
                print("Aborted")
                return
        # overwrite file
        with open(args.output, "w") as output_file:
            slice_paper(output_file, args)


def slice_paper(output_file, args):
    dotmatrices = generate_dotmatrices(args)

    # Generate Coordinate Lists using mode
    if args.mode == "dots":
        coordinate_lists = coordinates_dots(dotmatrices, args)
    elif args.mode == "horizontal":
        coordinate_lists = coordinates_horizontal(dotmatrices, args)
    else:
        coordinate_lists = coordinates_concentric(dotmatrices, args)

    # Write Start GCode
    output_file.write(f"""
; Created with PaperSlicer.py
; A script by Jens Welsch
; https://gitlab.com/jenswe/paperslicer

; Parameters: {args}
; Sliced {datetime.now().strftime("%Y-%m-%d %H:%M:%S")}

M73 P0 R{len(coordinate_lists) * 5 + 1} ; Reset Print Progress and set remaining time
M204 P{args.acceleration} T{args.acceleration} ; Set acceleration
G0 F{args.speed * 60} ; Set movement speed
G1 F{args.speed * 60} ; Set movement speed

M107    ; disable fan
M104 S0 ; Turn off the extruder heater
M140 S0 ; Turn off the bed heater
G90     ; use absolute coordinates
G28 W   ; home all without mesh bed level
""")
    if args.custom_start != "none":
        with open(args.custom_start) as start_file:
            output_file.write(start_file.read())
            output_file.write(f"\nG1 Z{args.lift + args.pen_offset[2]}\n")  # Enshure Pen is lifted

    paint_gcode(output_file, coordinate_lists[0], args)  # Print First image

    for i in range(1, len(coordinate_lists)): # Print all other images
        # Write Next Page GCode
        if args.custom_next_page != "none":
            with open(args.custom_next_page) as next_page_file:
                output_file.write(next_page_file.read())
                output_file.write("\n")

        paint_gcode(output_file, coordinate_lists[i], args)  # Print Dots

    # Write End GCode
    output_file.write("G1 Z20")
    if args.custom_end != "none":
        with open(args.custom_end) as end_file:
            output_file.write(end_file.read())
            output_file.write("\n")


def generate_dotmatrices(args):
    # Load input file with error handling
    if args.input.endswith(".pdf"):
        try:
            images = pdf2image.convert_from_path(args.input, dpi=args.resolution * 25.4)
        except Exception as e:
            print(f"Error: Could not open PDF {args.input}")
            print(e)
            return
    else:
        try:
            images = [Image.open(args.input)]
        except Exception as e:
            print(f"Error: Could not open image {args.input}")
            print(e)
            return

    # Define Variables
    dotmatrices = []  # List of boolean dotmatrices, where True means a dot is present
    size = (int(args.dimensions[0] * args.resolution),
            int(args.dimensions[1] * args.resolution))  # Size of the image in dots

    # Iterate over images
    for image in images:
        image = image.resize(size)  # Scale image
        dotmatrix = np.zeros(size, dtype=bool)  # Create blank dotmatrix

        for x in range(size[0]):
            for y in range(size[1]):
                # Image is top down, dotmatrix is bottom up
                dotmatrix[x, y] = is_color_dot(image.getpixel((x, size[1]-1 - y)), args)
        dotmatrices.append(dotmatrix)

    return dotmatrices


def coordinates_dots(dotmatrices, args):
    coordinate_lists = []
    for dotmatrix in dotmatrices:
        coordinate_list = []
        for y in range(dotmatrix.shape[1]):
            for x in range(dotmatrix.shape[0]):
                if dotmatrix[x, y]:
                    coordinate_list.append(image_to_paper_coord(x, y, args))
        coordinate_lists.append(coordinate_list)
    return coordinate_lists


def coordinates_horizontal(dotmatrices, args):
    coordinate_lists = []
    for dotmatrix in dotmatrices:
        coordinate_list = []
        for y in range(dotmatrix.shape[1]):
            start_x = None
            for x in (range(dotmatrix.shape[0]) if y % 2 == 0 else range(dotmatrix.shape[0] - 1, -1, -1)):
                if dotmatrix[x, y] and start_x is None:
                    start_x = x  # Start of a Line
                elif not dotmatrix[x, y] and start_x is not None:
                    coordinate_list.append(image_to_paper_coord(start_x, y, args))
                    coordinate_list.append(image_to_paper_coord(x - 1 if y % 2 == 0 else x + 1, y, args))
                    start_x = None  # End of a Line
            if start_x is not None:  # Line ends at the end of the row
                coordinate_list.append(image_to_paper_coord(start_x, y, args))
                coordinate_list.append(image_to_paper_coord(dotmatrix.shape[0] - 1, y, args))
        coordinate_lists.append(coordinate_list)
    return coordinate_lists


directions = [(1, 0), (1, 1), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1), (1, -1)]


def coordinates_concentric(dotmatrices, args):
    coordinate_lists = []
    for dotmatrix in dotmatrices:
        coordinate_list = []
        while dotmatrix.any():
            dir_i = 2  # Start moving up
            # Find first dot
            start = np.argwhere(dotmatrix)[0]
            start = (start[0], start[1])  # Convert to tuple
            line = [start]

            # check if no direction has a dot
            if not any([check_direction(dotmatrix, start, i) for i in range(8)]):
                dotmatrix[start[0], start[1]] = False
                coordinate_list.append(line)
                continue  # there is no line to draw, draw a single dot

            next_dot = None
            while next_dot != start:  # Until the line is a closed loop
                dir_i = (dir_i + 2) % 8  # Turn Left
                while not check_direction(dotmatrix, line[-1], dir_i):
                    dir_i = (dir_i - 1) % 8  # Turn Right until a dot is found
                # Take a step
                next_dot = (line[-1][0] + directions[dir_i][0], line[-1][1] + directions[dir_i][1])
                line.append(next_dot)

            coordinate_list.append(line)
            for x, y in line:
                dotmatrix[x, y] = False  # remove shell and draw inner shape

        # ToDo: Smooth Lines by adding 1 2 diagonals

        # use image_to_paper_coord for all dots
        for i, line in enumerate(coordinate_list):
            for j, dot in enumerate(line):
                coordinate_list[i][j] = image_to_paper_coord(dot[0], dot[1], args)
        coordinate_lists.append(coordinate_list)

    return coordinate_lists


def check_direction(dotmatrix, coordinate, dir_i):
    x = coordinate[0] + directions[dir_i][0]
    y = coordinate[1] + directions[dir_i][1]
    if x < 0 or x >= dotmatrix.shape[0] or y < 0 or y >= dotmatrix.shape[1]:
        return False
    return dotmatrix[x, y]


def paint_gcode(output_file, coordinate_list, args):
    if args.mode == "dots":
        for x, y in coordinate_list:  # TODO: Zig-zag lines to always move x   \/\/\/\/\/\/
            # Move to Dot
            output_file.write(
                f"G0 X{x + args.pen_offset[0]} Y{y + args.pen_offset[1]} Z{args.lift + args.pen_offset[2]}\n")
            # Paint Dot
            output_file.write(f"G1 Z{args.pen_offset[2]}\n")
            output_file.write(f"G1 Z{args.lift + args.pen_offset[2]}\n")

    elif args.mode == "horizontal":
        for i in range(0, len(coordinate_list), 2):
            x1, y1 = coordinate_list[i]  # Start
            x2, y2 = coordinate_list[i + 1]  # End
            # Move to Start
            output_file.write(
                f"G0 X{x1 + args.pen_offset[0]} Y{y1 + args.pen_offset[1]} Z{args.lift + args.pen_offset[2]}\n")
            # Paint Line
            output_file.write(f"G1 Z{args.pen_offset[2]}\n")
            output_file.write(f"G1 X{x2 + args.pen_offset[0]} Y{y2 + args.pen_offset[1]}\n")
            output_file.write(f"G1 Z{args.lift + args.pen_offset[2]}\n")

    else:
        for line in coordinate_list:
            # Move to first Dot
            output_file.write(
                f"G0 X{line[0][0] + args.pen_offset[0]} Y{line[0][1] + args.pen_offset[1]} Z{args.lift + args.pen_offset[2]}\n")
            # Start Line
            output_file.write(f"G1 Z{args.pen_offset[2]}\n")
            for x, y in line:  # Draw Line
                output_file.write(f"G1 X{x + args.pen_offset[0]} Y{y + args.pen_offset[1]}\n")
            # End Line
            output_file.write(f"G1 Z{args.lift + args.pen_offset[2]}\n")


def image_to_paper_coord(x, y, args):
    return (args.start_coordinates[0] + x / args.resolution,
            args.start_coordinates[1] + y / args.resolution)


def is_color_dot(color, args):
    # if color is tupel, get first value
    if isinstance(color, tuple):
        color = color[0]
    return color < 128


def check_file_exists(file):
    try:
        with open(file):
            pass
    except FileNotFoundError:
        print(f"Error: File {file} not found")
        return False
    return True


if __name__ == "__main__":
    main()
